# nicomanga_feed
ニコニコ漫画の更新をatomフィードにします。

## Install
For Ubuntu 22.04 and apache

```sh
$ cd /var/www 
$ sudo git clone https://github.com/sanadan/nico3d_feed.git
$ cd nicomanga_feed
$ sudo ./setup
```

設置したサーバーの nicomanga_feed にアクセスすると、フィードが出力されます。

フィードは約1時間毎に更新されます。

## License
MIT

## Copyright
Copyright 2015-2024 sanadan <jecy00@gmail.com>
